<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

$userIP = $_SERVER['REMOTE_ADDR'];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="entry-content">
		<section>
			<p class="lead">Your IP address is currently: <span class="label label-default"><?=$userIP?></span>
			<p>IPv6 status: <span id="ipv6-status">checking…</span></p>
			<ul class="nav nav-tabs nav-justified">
				<li><a rel="external" href="http://network-tools.com/default.asp?prog=express&amp;host=<?=$userIP?>">Network Tools Report</a>
				<li><a rel="external" href="http://www.robtex.com/ip/<?=$userIP?>.html">Robtex</a>
				<li><a rel="external" href="http://www.ip-adress.com/reverse_ip/<?=$userIP?>">Reverse Lookup</a>
				<li><a rel="external" href="http://just-ping.com/index.php?vh=<?=$userIP?>&amp;c=&amp;s=ping!">JustPing</a>
			</ul>
		</section>

		<?php if(!empty($host)){ ?>
		<section>
			<h3>Host</h3>
				<p class="lead">Your hostname is currently: <?=$host?> (often just your IP)</p>
			<ul>
				<li><a rel="external" href="http://whois.domaintools.com/<?=$host?>">Whois</a>
				<li><a rel="external" href="http://www.whatsmydns.net/#A/<?=$host?>">Name Servers</a>
				<li><a rel="external" href="http://www.whatsmydns.net/#MX/<?=$host?>">Mail Servers</a>
			</ul>
		</section>
		<?php }; ?>

		<?php the_content(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'trippnology' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

