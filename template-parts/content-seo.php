<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="featurette row">
			<div class="col-sm-push-9 col-sm-3">
				<img class="featurette-image img-circle"
					srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-256.png 256w,
							<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-512.png 512w"
					sizes="256px, (min-width:768px) 158px, (min-width:992px) 213px, (min-width:1200px) 263px"
					src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-256.png"
					alt="Search engine optimisation Attleborough icon"
				/>
			</div>
			<div class="col-sm-pull-3 col-sm-9">
				<h1>Is your website hard to find?</h1>
				<p class="lead">Search engine optimisation, or simply SEO, is the art of getting your pages ranked high in the search engine results. Trippnology can optimise your existing website and suggest new content to bring in those extra customers.</p>
				<p>85% of internet users use Google for their searching so it's vital that your website is on the first page. In fact, you probably found this very article via a Google search.</p>
				<p>While Google certainly has the lion's share of the search market, old favorites like Yahoo and Microsoft's Bing are also very important, so we make sure your content will be well indexed by all.</p>
				<p>We provide you with our custom Google Analytics dashboards which show you the metrics that matter, helping you to further tune your pages and proving that hiring us is good for your business.</p>
				<p>All SEO work is charged at our flat web services rate available on our <a href="<?php echo esc_url( home_url( '/prices/' ) ); ?>">web services pricing</a> page.</p>
				<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Contact us</a> about search engine optimisation today and boost your online presence.</p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="featurette row">
			<div class="col-sm-push-9 col-sm-3">
				<?php /* TODO: Use responsive image
				<img class="featurette-image img-circle"
					srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-256.png 256w,
							<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-512.png 512w"
					sizes="256px, (min-width:768px) 158px, (min-width:992px) 213px, (min-width:1200px) 263px"
					src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/search-icon-256.png"
					alt="Search engine optimisation Attleborough icon"
				/>
				*/ ?>
				<img src="http://trippnology.com/img/screens/limecrete.jpg" alt="Screenshot of the Limecrete Company website" class="roundsm img-polaroid featurette-image">
			</div>
			<div class="col-sm-pull-3 col-sm-9">
				<h1>Case Study</h1>
				<p class="lead">Trippnology were approached by ecological construction firm <a href="http://limecrete.co.uk/" rel="external">The Limecrete Company</a> who provide <a href="http://limecrete.co.uk/limecrete/" rel="external">limecrete</a>, <a href="http://limecrete.co.uk/hempcrete/" rel="external">hempcrete</a> and <a href="http://limecrete.co.uk/polished-screed/" rel="external">polished screed</a> throughout Europe.</p>
				<p>Louisa and Myles already had a website they had built themselves	using a popular DIY site builder. As with most of these systems, it got	them online but the pages produced were far from search engine friendly	which was hurting their quest for new customers.</p>
				<p>We completely overhauled the site, simplifying the code and optimising all of the content to improve loading speed, boost their search ranking and pull in new business.</p>
				<p>Since the redesign, week after week, visitor numbers have continued to grow. Louisa tells us that the number of enquiries tripled almost immediately, leading to them having to take on extra staff to cope with demand for their services.</p>
				<p>Do you want more customers landing on your website? <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Contact us</a> today.</p>
			</div>
		</div>


	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

