<?php
/**
 * Template part for displaying content in page-it-support.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="row">
			<div class="col-sm-12 col-md-8">
				<h1>Things go wrong and technology can play up sometimes.</h1>
				<p>If something goes wrong with your IT, it can leave your staff unable to continue with their work. This where we come in, with our passion for problem solving. Our customers soon realise we are the people to call whenever issues crop up.</p>
				<p>We've built up a wealth of real world experience over many years of diagnosing problems with hardware, software (both off the shelf and bespoke), operating systems, networks, broadband and user behaviour. This experience allows us to quickly pinpoint the issue, minimising costly downtime.</p>
				<p>Hiring Trippnology to provide your IT support gives you access to our expertise at a <a href="prices">fraction of the cost</a> of employing full time staff. Both monthly contracts and ad-hoc support are available.</p>
				<h3 class="text-center">&ldquo;That's exactly what I need!&rdquo; <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn btn-lg btn-success">SEND HELP!</a></h3>
			</div>
			<div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3 well white">
				<p><span class="lead">Bottom line:</span> IT downtime costs your business. If you need emergency IT support in Attleborough, we could be there within an hour!</p>
				<p class="text-center"><a class="btn btn-sm btn-success" href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">SEND HELP!</a></p class="text-center">
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="row">
			<div class="col-sm-12 col-md-8">
				<h1>Not just for the bad times!</h1>
				<p>We have our finger on the pulse of the IT world, keeping up to date with the latest hardware and software so you don't have to.</p>
				<p>If your business relies on the internet for its daily operations, you need your connection to be reliable and for any faults that do occur to be quickly rectified. Trippnology can order, install and setup business grade broadband, ensuring your business stays connected.</p>
				<p>Perhaps you're looking to expand and would like some purchasing advice. Or have some equipment that you're not using to its full potential. We can help with that too, showing you the right tools for the job and making the most of your existing IT investment.</p>
				<h3 class="text-center">&ldquo;I need expert IT advice.&rdquo; <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn btn-lg btn-success">Get in touch</a></h3>
				<p class="text-center"><a href="http://www.computerrepaircompanies.co.uk/company/trippnology/" rel="nofollow">Recommended Computer Repair Company</a></p>
			</div>
			<div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-3 well white">
				<p><span class="lead">Bottom line:</span> If you're looking to add some new equipment, improve your broadband or just need some advice, we're here to help.</p>
				<p class="text-center"><a class="btn btn-sm btn-success" href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Let's talk »</a></p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="row">
			<div class="col-sm-12">
			<h2>IT Support Service Area</h2>
				<p>We mainly serve <span itemscope itemtype="http://schema.org/Place" itemprop="name">Attleborough</span>, <span itemscope itemtype="http://schema.org/Place" itemprop="name">Wymondham</span>, <span itemscope itemtype="http://schema.org/Place" itemprop="name">Norwich</span> and <span itemscope itemtype="http://schema.org/Place" itemprop="name">Thetford</span> but we also cover surrounding towns and villages. The map below serves as a rough guide.</p>
				<div id="googlemap">
					<iframe width="100%" scrolling="no" height="500" frameborder="0" src="https://maps.google.co.uk/maps?cid=17370591561013489825&amp;ssa=1&amp;ie=UTF8&amp;hq=+loc:+&amp;hnear=&amp;source=embed&amp;ll=52.538703,1.005369&amp;spn=0.292462,0.673827&amp;iwloc=A&amp;output=embed" marginwidth="0" marginheight="0"></iframe>
					<p><a target="_blank" href="https://maps.google.co.uk/maps?cid=17370591561013489825&amp;ssa=1&amp;ie=UTF8&amp;hq=+loc:+&amp;hnear=&amp;source=embed&amp;ll=52.538703,1.005369&amp;spn=0.292462,0.673827&amp;iwloc=A">View Larger Map</a></p>
				</div>
			</div>
		</div>


	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

