<?php
/**
 * Template part for displaying single projects.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<img src="<?php the_field('screenshot'); ?>" alt="Screenshot of the <?php the_title(); ?> website">
		<?php the_content(); ?>
		<p>URL: <a href="<?php the_field('url'); ?>"><?php the_field('url'); ?></a></p>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'trippnology' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php trippnology_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

