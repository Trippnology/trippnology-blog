<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<p class="lead">Hello there! Nice to meet you!</p>
		<p class="lead">Here's a little explanation of what we're all about:</p>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="row entry-content">
		<div class="col-sm-12 col-lg-offset-1 col-lg-10">
			<?php the_content(); ?>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

