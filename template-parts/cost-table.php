<p>How much does it cost to make a website? The following shows what you can expect to spend, depending on your needs.</p>
<?php // List for small screens ?>
<div class="row visible-xs">
	<div class="well white">
		<ul class="list-unstyled">
			<li>Logos and brands: <strong>&pound;50-500</strong></li>
			<li>A simple website: <strong>&pound;500-1000</strong></li>
			<li>A website with a bit where you can log in and change the content on the page: <strong>&pound;1000-2500</strong></li>
			<li>A larger website where you can list and sell things: <strong>&pound;2500-3500</strong></li>
			<li>*A mucky website with videos of muffs, knockers and winkies: <strong>&pound;3500-4000</strong></li>
		</ul>
	</div>
</div>
<?php // Table for large screens ?>
<table class="table hidden-xs price-table-web">
	<tr>
		<th style="width:12.5%;">&nbsp;</th>
		<th style="width:25%;" colspan="2">&pound;1000</th>
		<th style="width:25%;" colspan="2">&pound;2000</th>
		<th style="width:25%;" colspan="2">&pound;3000</th>
		<th style="width:12.5%;">&pound;4000</th>
	</tr>
	<tr>
		<td style="width:12.5%;" class="burgundy">Logos and brands</td>
		<td style="width:12.5%;" class="grape">A simple website</td>
		<td style="width:25%;" colspan="3" class="brightorange">A website with a bit where you can log in and change the content on the page.</td>
		<td style="width:25%;" colspan="2" class="lightorange">A larger website where you can list and sell things.</td>
		<td style="width:25%;" class="grey">*A mucky website with videos of muffs, knockers and winkies.</td>
	</tr>
</table>
<p>*We've never built one of these, we just threw it in for a bit of spice.</p>
