<?php
/**
 * Template part for displaying page content in page-web-design.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<hr class="divider large roundsm">

		<div class="row intro">
			<div class="col-sm-4">
				<img class="featurette-image-mini img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/portfolio-icon-256.png" alt="it support">
				<p class="featurette-heading-mini">Portfolio</p>
				<p>Have a look at some of the sites we have built for previous customers and see the quaility of our work for yourself.</p>
				<p><a class="scrollme btn btn-default" href="#portfolio">View »</a></p>
			</div>
			<div class="col-sm-4">
				<img class="featurette-image-mini img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/code-icon-256.png" alt="project assessment">
				<p class="featurette-heading-mini">Brand new project</p>
				<p>Need a website but don't know where to start? Our project assessment will get you on the right track.</p>
				<p><a class="scrollme btn btn-default" href="#project-assessment">Find out more »</a></p>
			</div>
			<div class="col-sm-4">
				<img class="featurette-image-mini img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/stats-icon-256.png" alt="website updates">
				<p class="featurette-heading-mini">Website maintenance</p>
				<p>Do you already have a website that needs some TLC? We can breathe new life into your stale old site.</p>
				<p><a class="scrollme btn btn-default" href="#website-maintenance">View details »</a></p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="row">
			<div class="col-sm-6">
				<p>A website gives you one shot at making an impression. If people find it difficult to read or spend ages waiting for information to download, they’ll close their browser and you’ll probably never hear from them again.</p>
				<p>We plan, design and build websites that collect customers rather than losing them.</p>
				<p>Created with SEO (<a href="<?php echo esc_url( home_url( '/seo/' ) ); ?>">Search Engine Optimisation</a>) in mind to help drive traffic, our sites retain interest using eye-catching writing and website design that is easy to read and navigate.</p>
			</div>
			<div class="col-sm-6">
				<p>As part of our website development service, all sites are tested across different browsers and platforms and structured so that visits are converted into sales or meeting requests.</p>
				<p>We can also provide full content management systems so your site is easy to update, a social media strategy to start you off on Twitter or Facebook and help with hosting and buying a domain name.</p>
				<p>There are 51 million internet users in the UK right now.</p>
				<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Make sure more of them start visiting your site.</a></p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<section id="portfolio">
			<h2>Portfolio</h2>
			<p><span class="lead">Key:</span> <i class="glyphicon glyphicon-tags"></i> = HTML5 | <i class="glyphicon glyphicon-sunglasses"></i> = NodeJS | <i class="glyphicon glyphicon-resize-full"></i> = Responsive | <span class="glyphicon glyphicon-cog"></span> = Web App | <i class="glyphicon glyphicon-pencil"></i> = Wordpress</p>
			<div class="row">
			<?php
				$args = array(
					'post_type'      => 'project',
					'posts_per_page' => -1,
					'meta_key' => 'date',
					'orderby' => 'meta_value_num',
				);
				$projects = new WP_Query( $args );

			    while ($projects->have_posts()) :
			    	$projects->the_post();
			    	get_template_part( 'template-parts/project', 'single' );
			    endwhile; ?>

			    <?php wp_reset_postdata(); ?>
			</div>
		</section>

		<hr class="divider large roundsm">

		<section id="project-assessment">
			<h2>Project Assessment</h2>
			<div class="row">
				<div class="col-sm-8 col-lg-9">
					<p>A project assessment, as we’ve defined it, is a detailed plan for the work that is to be done on a project, and explains how we do it. We eliminate the guess work, and detail the project out to such a level that this document becomes a living part of the development process, being referred back to and acting as the guide towards the project’s successful completion.</p>
					<p>These details are valuable to both you (in making sure you know exactly what you’re getting) and to us (in making sure that we know exactly what we need to deliver).</p>
					<p>Before we can build you a website, we need to know what it is that you have in mind. The only way to give you an accurate quote is to understand what your needs are and what you are trying to achieve. You may not know this yourself initially, but we'll talk you through it and help you set appropiate goals.</p>
					<p>Putting this together takes time and includes a 1 hour meeting with you (in person or via the web) so <a href="//trippnology.com/prices/#web-design">we do charge for this</a>. Once you have your completed project assessment, you are under no obligation to go ahead with the build, but if you do, the cost of the assessment will be refunded.</p>
					<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Contact us</a> today and let's get the ball rolling!</p>
				</div>
				<div class="col-sm-4 col-lg-3">
					<div class="well white">
						<p><span class="lead">Bottom line:</span> We assess your idea and give you a plan to make it a reality. If you go ahead with the project, this service is free of charge.</p>
						<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn btn-mini btn-success">Let's talk »</a></p>
					</div>
				</div>
			</div>
		</section>

		<hr class="divider large roundsm">

		<section id="website-maintenance">
			<h2>Website Maintenance</h2>
			<div class="row">
				<div class="col-sm-8 col-lg-9">
					<p>As users of the web, we're no longer chained to our desks. The use of smartphones and tablets is soaring and many older websites have problems displaying on mobile devices, and in some cases, are entirely broken.</p>
					<p>The landcape of the web is ever changing. New technologies emerge, and with hindsight, we sometimes realise that the way we built the web in the past was less than ideal. For example, the <code>&lt;table&gt;</code> element (intended for displaying tabular data) used to be abused to provide page layout. Yuck.</p>
					<p>Here's where we come in. We'll fix up those old pages and make sure your customers aren't frustrated when using your site. We'll bring your website up to modern standards and make it more accessible to everyone.</p>
					<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">Contact us</a> today and let's get rid of those cobwebs!</p>
				</div>
				<div class="col-sm-4 col-lg-3">
					<div class="well white span2 pull-right">
						<p><span class="lead">Bottom line:</span> We can bring your current website up to scratch and make it mobile friendly.</p>
						<p><a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>" class="btn btn-mini btn-success">Let's talk »</a></p>
					</div>
				</div>
			</div>
		</section>

		<hr class="divider large roundsm">

		<section>
			<h2>Local</h2>
			<p class="lead">Areas we work in most often:</p>
			<ul class="list-inline">
				<li itemscope="" itemtype="http://schema.org/Place"><span itemprop="name">Attleborough</span></li>
				<li itemscope="" itemtype="http://schema.org/Place"><span itemprop="name">Wymondham</span></li>
				<li itemscope="" itemtype="http://schema.org/Place"><span itemprop="name">Norwich</span></li>
				<li itemscope="" itemtype="http://schema.org/Place"><span itemprop="name">Thetford</span></li>
			</ul>
		</section>
		<section>
			<h2>Skill Set</h2>
			<p class="lead">Technology we use to build the web:</p>
			<ul class="list-inline">
				<li>HTML5</li>
				<li>CSS3</li>
				<li>Javascript</li>
				<li>PHP</li>
				<li>NodeJS</li>
				<li>RESTful APIs</li>
				<li>Firefox OS</li>
				<li>Wordpress</li>
				<li>jQuery</li>
				<li>AJAX</li>
				<li>MySQL</li>
				<li>MongoDB</li>
				<li>JSON</li>
				<li>XML</li>
			</ul>
		</section>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

