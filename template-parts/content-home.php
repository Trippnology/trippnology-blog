<?php
/**
 * Template part for displaying page content in page-home.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="featurette row">
			<div class="col-sm-push-9 col-sm-3">
				<img class="featurette-image img-circle"
					srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/code-icon-256.png 256w,
							<?php echo get_stylesheet_directory_uri(); ?>/img/ui/code-icon-512.png 512w"
					sizes="(min-width:1200px) 263px, (min-width:992px) 213px, (min-width:768px) 158px, 256px"
					src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/code-icon-256.png"
					alt="Web design Attleborough icon"
				/>
			</div>
			<div class="col-sm-pull-3 col-sm-9">
				<h2 class="featurette-heading">We build websites. <br class="visible-md visible-lg"><span class="text-muted">Pretty darn good ones too.</span></h2>
				<p class="lead">Driven by our passion for the web, we create responsive, user focussed sites that meet your needs and build your business. Check out our <a href="<?php echo esc_url( home_url( '/web-design/#portfolio' ) ); ?>">portfolio</a>, find out more about our <a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>">web design</a> services or <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a> today, we'd love to hear your plans.</p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="featurette row">
			<div class="col-sm-3">
				<img class="featurette-image img-circle"
					srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/stats-icon-256.png 256w,
							<?php echo get_stylesheet_directory_uri(); ?>/img/ui/stats-icon-512.png 512w"
					sizes="(min-width:1200px) 263px, (min-width:992px) 213px, (min-width:768px) 158px, 256px"
					src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/stats-icon-256.png"
					alt="Web design Attleborough icon"
				/>
			</div>
			<div class="col-sm-9">
				<h2 class="featurette-heading">Already got a website? <br class="visible-md visible-lg"><span class="text-muted">Let's make it better.</span></h2>
				<p class="lead">All websites need a little work over time, as content gets updated and new technologies emerge. With our <a href="<?php echo esc_url( home_url( '/web-design/#website-maintenance' ) ); ?>">website maintenance</a> service, your site will bring in more visitors, which means more customers!</p>
			</div>
		</div>

		<hr class="divider large roundsm">

		<div class="featurette row">
			<div class="col-sm-push-9 col-sm-3">
				<img class="featurette-image img-circle"
					srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/it-support-icon-256.jpg 256w,
							<?php echo get_stylesheet_directory_uri(); ?>/img/ui/it-support-icon-512.jpg 512w"
					sizes="(min-width:1200px) 263px, (min-width:992px) 213px, (min-width:768px) 158px, 256px"
					src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/it-support-icon-256.jpg"
					alt="Web design Attleborough icon"
				/>
			</div>
			<div class="col-sm-pull-3 col-sm-9">
				<h2 class="featurette-heading">Fed up with IT problems? <br class="visible-md visible-lg"><span class="text-muted">We're here to help.</span></h2>
				<p class="lead">IT downtime costs your business. Our years of experience mean we get your issues resolved quickly, so you can get back to work. Find out more about how our <a href="<?php echo esc_url( home_url( '/it-support/' ) ); ?>">IT support</a> services will make you more productive.</p>
			</div>
		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->

