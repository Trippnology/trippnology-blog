<?php // Eliminating roundtrips with preconnect https://www.igvita.com/2015/08/17/eliminating-roundtrips-with-preconnect/ ?>
<link rel="preconnect" href="https://cdn.trippnology.com" crossorigin>
<link rel="preconnect" href="https://gist.github.com" crossorigin>
<link rel="preconnect" href="https://assets-cdn.github.com" crossorigin>
<link rel="preconnect" href="https://ssl.google-analytics.com" crossorigin>
<link rel="preconnect" href="http://s.w.org" crossorigin>
