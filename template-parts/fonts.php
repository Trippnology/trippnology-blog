<style>
	@font-face {
		font-family: "MetaPro-Bold";
		src: url('https://cdn.trippnology.com/fonts/metapro-bold.eot');
		src: url('https://cdn.trippnology.com/fonts/metapro-bold.eot?#iefix') format('embedded-opentype'),
			 url('https://cdn.trippnology.com/fonts/metapro-bold.svg#metapro-bold') format('svg'),
			 url('https://cdn.trippnology.com/fonts/metapro-bold.woff') format('woff'),
			 url('https://cdn.trippnology.com/fonts/metapro-bold.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: "museo_sans_300regular";
		src: url('https://cdn.trippnology.com/fonts/museosans-300-webfont.eot');
		src: url('https://cdn.trippnology.com/fonts/museosans-300-webfont.eot?#iefix') format('embedded-opentype'),
			 url('https://cdn.trippnology.com/fonts/museosans-300-webfont.svg#museo_sans_300regular') format('svg'),
			 url('https://cdn.trippnology.com/fonts/museosans-300-webfont.woff') format('woff'),
			 url('https://cdn.trippnology.com/fonts/museosans-300-webfont.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}
</style>
