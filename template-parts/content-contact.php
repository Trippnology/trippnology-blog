<?php
/**
 * Template part for displaying page content in page-contact.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if (!is_page('home')) {?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
<?php } ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<section class="contact-info" itemscope itemtype="http://schema.org/LocalBusiness">
					<h2><span itemprop="name"><?php bloginfo( 'name' ); ?></span></h2>
					<p><span itemprop="description">You love your business, we love the web. <br class="hidden-md">Let's get together and make something great!</span></p>
					<p>Email: <span itemprop="email" class="antispam"><?php the_field( 'email' ); ?></span></p>
					<p>Phone: <span itemprop="telephone"><a href="tel:<?php the_field( 'tel-machine' ); ?>"><?php the_field( 'tel-human' ); ?></a></span><br>
						Mobile: <a href="tel:<?php the_field( 'mob-machine' ); ?>"><?php the_field( 'mob-human' ); ?></a></p>
					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<address><span itemprop="streetAddress"><?php the_field( 'streetAddress' ); ?></span><br>
						<span itemprop="addressLocality"><?php the_field( 'addressLocality' ); ?></span><br>
						<span itemprop="addressRegion"><?php the_field( 'addressRegion' ); ?></span><br>
						<span itemprop="postalCode"><?php the_field( 'postalCode' ); ?></span><br>
						</address>
					</div>
					<p><a rel="external" itemprop="map" href="<?php the_field( 'map' ); ?>" class="badge popup-map"><span class="glyphicon glyphicon-map-marker"></span> Map</a></p>
					<meta itemprop="url" content="<?php echo esc_url( home_url( '/' ) ); ?>">
				</section>
				<section class="hidden-xs qrcode">
					<p>Scan this code to add contact info to your phone, call us, or get directions.</p>
					<div class="center">
						<img class="img-polaroid" src="https://cdn.trippnology.net/img/contact-details-qrcode.png" alt="Trippnology contact details QR code">
						<p class="small"><a href="https://en.wikipedia.org/wiki/QR_code" target="_blank">What is this?</a></p>
					</div>
				</section>
			</div>
			<div class="col-sm-4 col-sm-offset-2 col-lg-3">
				<div class="text-center">
					<img class="featurette-image-mini img-circle"
						srcset="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/hi-icon-256.png 256w,
								<?php echo get_stylesheet_directory_uri(); ?>/img/ui/hi-icon-512.png 512w"
						sizes="128px"
						src="<?php echo get_stylesheet_directory_uri(); ?>/img/ui/hi-icon-256.png"
						alt="Hi icon"
					/>
					<?php echo do_shortcode( '[contact-form-7 id="526" html_class="form-vertical"]' ); ?>
				</div>
			</div>
		</div> <!-- /row -->
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'trippnology' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

