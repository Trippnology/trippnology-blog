
<div class="col-sm-4 col-lg-3 portfolio-item">
	<img class="portfolio-image roundlrg border-light" src="<?php the_field('screenshot'); ?>" alt="Screenshot of the <?php the_title(); ?> website">
	<p class="project-link"><a href="<?php the_field('url'); ?>" rel="external"><?php the_title(); ?></a></p>
	<?php
	the_content();

	$posttags = get_the_tags();
	if ($posttags) {
		echo '<ul class="list-inline project-tags">';
		foreach($posttags as $tag) {
			switch ($tag->name) {
				case 'html5':
					$icon = 'glyphicon-tags';
					break;
				case 'nodejs':
					$icon = 'glyphicon-sunglasses';
					break;
				case 'responsive':
					$icon = 'glyphicon-resize-full';
					break;
				case 'webapp':
					$icon = 'glyphicon-cog';
					break;
				case 'wordpress':
					$icon = 'glyphicon-pencil';
					break;
				default:
					$icon = '';
					break;
			}
			echo '<li><span class="project-tag" title="'.$tag->name.'"><span class="glyphicon ' . $icon . '"></span></span></li>';
		}
		echo '</ul>';
	}?>
</div>
