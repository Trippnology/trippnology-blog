<?php
/**
 * The template for displaying the IT support page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div id="primary" class="col-sm-12 content-area">
			<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'it-support' ); // Just the title ?>

			<?php endwhile; // End of the loop. ?>

			</main>
		</div><!-- #primary -->
	</div>
</div>
<?php get_footer(); ?>
