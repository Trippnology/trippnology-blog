<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trippnology
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<hr class="divider roundsm large">
		<div class="container">
			<div class="row site-info">
				<div class="col-sm-4">
					<p class="lead">Most popular:</p>
					<ul class="list-unstyled">
						<li><a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>">Web design Attleborough</a></li>
						<li><a href="<?php echo esc_url( home_url( '/seo/' ) ); ?>">SEO Attleborough</a></li>
						<li><a href="<?php echo esc_url( home_url( '/it-support/' ) ); ?>">IT support Attleborough</a></li>
					</ul>
				</div>
				<div class="col-sm-4">
					<p class="lead">Social</p>
					<ul class="list-unstyled">
						<li><a href="https://twitter.com/Trippnology" rel="external">Twitter</a></li>
						<li><a href="https://github.com/Trippnology" rel="external">GitHub</a></li>
						<li><a href="https://bitbucket.org/Trippnology" rel="external">Bitbucket</a></li>
					</ul>
				</div>
				<div class="col-sm-4">
					<p class="lead">Meta</p>
					<ul class="list-unstyled">
						<li><a href="<?php echo esc_url( home_url( '/privacy-policy/' ) ); ?>">Privacy Policy</a></li>
						<li><a href="<?php echo esc_url( home_url( '/terms-and-conditions/' ) ); ?>">Terms and Conditions</a></li>
						<li><a href="<?php echo esc_url( home_url( '/sitemap_index.xml' ) ); ?>" rel="sitemap">Sitemap</a></li>
						<li class="license"><a href="http://creativecommons.org/licenses/by-sa/2.0/uk/" rel="license"><img src="https://cdn.trippnology.com/img/cc-by-sa-80x15.png" alt="Creative Commons Attribution-ShareAlike 2.0 UK: England &amp; Wales license"></a> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Trippnology">Trippnology</a></li>
						<li class="ipv6"><a href="http://ipv6-test.com/validate.php?url=referer" rel="external"><img src="https://cdn.trippnology.com/img/ipv6-80x15.png" alt="ipv6 ready"></a></li>
					</ul>
				</div>
			</div><!-- .site-info -->
			<div class="row">
				<div class="span12">
					<?php get_template_part('template-parts/review-score'); ?>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
