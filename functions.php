<?php
/**
 * Trippnology functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Trippnology
 */

if ( ! function_exists( 'trippnology_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function trippnology_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Trippnology, use a find and replace
	 * to change 'trippnology' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'trippnology', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Remove generator meta tag
	//remove_action('wp_head', 'wp_generator');

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Setup menus
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'trippnology' ),
		'blog' => esc_html__( 'Blog Menu', 'trippnology' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	/*add_theme_support( 'custom-background', apply_filters( 'trippnology_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );*/

	// Modify comment form fields https://stackoverflow.com/a/19798621
	/*function modify_comment_fields($fields){
		$fields   =  array(
				'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
				            '<input id="author" name="author" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . $html_req . ' /></p>',
				'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
				            '<input id="email" name="email" class="form-control" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>',
				'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website' ) . '</label> ' .
				            '<input id="url" name="url" class="form-control" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
		);
	    return $fields;
	  }
	  add_filter('comment_form_default_fields','modify_comment_fields');*/

	  // Add custom editor styles to match the front end
	  //add_editor_style('https://cdn.trippnology.com/css/bootstrap-3.3.6.min.css');
	  add_editor_style('style.css');
}
endif; // trippnology_setup
add_action( 'after_setup_theme', 'trippnology_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function trippnology_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'trippnology_content_width', 640 );
}
add_action( 'after_setup_theme', 'trippnology_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function trippnology_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'trippnology' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'trippnology_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function trippnology_scripts() {
	wp_enqueue_style( 'bootstrap', 'https://cdn.trippnology.com/css/bootstrap-3.3.7.min.css', null, null );
	// Main theme CSS
	wp_enqueue_style( 'trippnology-style', get_stylesheet_uri(), array('bootstrap'), null );

	// Remove the default jQuery from the head
	//wp_deregister_script('jquery');
	//wp_deregister_script('jquery-migrate');
	//wp_enqueue_script('jquery', 'https://cdn.trippnology.com/js/jquery-1.12.1.min.js', null, null, true );
	wp_enqueue_script('bootstrap', 'https://cdn.trippnology.com/js/bootstrap-3.3.7.min.js', array('jquery'), null, true );
	wp_enqueue_script('jquery-plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery', 'bootstrap'), null, true);
	// Main theme JS
	wp_enqueue_script( 'theme', get_template_directory_uri() . '/js/theme.js', array('jquery', 'bootstrap', 'jquery-plugins'), null, true );
	// Load snow script if we're in December
	if ( "12" === date('n') ) {
		wp_enqueue_script( 'snow', get_template_directory_uri() . '/js/snow.js', array(), '0.1', true );
	}

	wp_enqueue_script( 'trippnology-navigation', get_template_directory_uri() . '/js/navigation.js', array(), null, true );
	wp_enqueue_script( 'trippnology-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_page('shed') ) {
		wp_enqueue_script( 'trippnology-shed', get_template_directory_uri() . '/js/shed.js', array('jquery'), null, true );
	}

	if ( is_page('make-link') ) {
		wp_enqueue_script( 'trippnology-make-link', get_template_directory_uri() . '/js/make-link.js', array('jquery'), null, true );
	}

	if ( is_page('youtube-mature-content-viewer') ) {
		wp_enqueue_script( 'trippnology-youtube-mature', get_template_directory_uri() . '/js/youtube-mature.js', array('jquery'), null, true );
	}
}
add_action( 'wp_enqueue_scripts', 'trippnology_scripts' );

/*// Remove WP version number from js and css files.
function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );*/

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
