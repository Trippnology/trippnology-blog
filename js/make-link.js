$(document).ready(function() {
	$('#linkform').submit(function(e){
		// Stop the form from being submitted
		e.preventDefault();
		// Grab the user input
		var url = $('#urlinput').val();
		// Button that will download this link
		var linkbutton = $('<a>',{
			'id': 'yourlink',
			'class': 'btn btn-success',
			'rel': 'external',
			'download': '',
			'text': url,
			'href': url
		});
		// Stuff into the page
		$('#linkholder').html(linkbutton);
	});
});
