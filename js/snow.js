function snow() {
	/* Snow Parameters */
	var count = 75;
	var wind = { x: 2, y: 1 };

	var PI2 = Math.PI * 2;

	var particles = [];
	var width = window.innerWidth;
	var height = window.innerHeight;
	var halfWidth = width / 2;
	var mouse = { x: 0, y: 0 };
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	canvas.className = 'snow';

	setup();

	function setup() {
		for(var i = 0; i < count; i++) {
			particles.push({
				x: Math.random() * width,     // x-pos
				y: Math.random() * height,    // y-pos
				size: 1 + Math.random() * 3,    // radius
				weight: Math.random() * count, // density
				angle: Math.random() * 360
			});
		}

		handleResize();

		window.addEventListener('resize', handleResize);
		window.addEventListener('mousemove', handleMouseMove);

		if(window.orientation !== undefined) {
			window.addEventListener('deviceorientation', handleDeviceOrientation);
		}

		document.body.insertBefore(canvas, document.body.firstChild);
		window.requestAnimationFrame(render);
	}

	function handleResize() {
		width = window.innerWidth;
		height = window.innerHeight;
		halfWidth = width / 2;
		canvas.width = width;
		canvas.height = height;
	}

	function handleMouseMove(e) {
		mouse.x = e.x || e.clientX;
		mouse.y = e.y || e.clientY;
		wind.x = map(mouse.x - halfWidth, -halfWidth, halfWidth, 4, -4);
	}

	var once = true;

	function handleDeviceOrientation(e) {
		// Remove the mouse event listener and only use gyro
		if(e.gamma !== null) {
			if(!(window.orientation % 180)) {
				wind.x = map(e.gamma, -60, 60, -4, 4);
			} else {
				wind.x = map(e.beta, -60, 60, 4, -4);
			}

			if(once) {
				window.removeEventListener('mousemove', mousemove);
				once = false;
			}
		}
	}

	function render() {
		ctx.clearRect(0, 0, width, height);
		ctx.fillStyle = 'rgba(250,250,250,0.8)';
		ctx.beginPath();
		for(var i = 0; i < count; i++) {
			var particle = particles[i];
			ctx.moveTo(particle.x, particle.y);
			ctx.arc(particle.x, particle.y, particle.size, 0, PI2, true);
		}
		ctx.fill();
		update();
		requestAnimationFrame(render);
	}

	function map(x, in_min, in_max, out_min, out_max) {
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	function update() {
		for(var i = 0; i < count; i++) {
			var particle = particles[i];
			particle.angle += 0.01;
			particle.y += Math.cos(particle.weight) + wind.y + particle.size / 2;
			particle.x += Math.sin(particle.angle) + wind.x;

			if(particle.x > width + 5 || particle.x < -5 ||	particle.y > height) {
				if(i % 3 > 0) {
					particle.x = Math.random() * width;
					particle.y = -5;
				} else {
					//If the flake is exitting from the right
					if(particle.x > halfWidth) {
						//Enter from the left
						particle.x = -5;
						particle.y = Math.random() * height;
					} else {
						//Enter from the right
						particle.x = width + 5;
						particle.y = Math.random() * height;
					}
				}
			}
		}
	}
}
// Only activate if we are in December
if ((mnth = new Date().getMonth()) === 11 || mnth === 0) {
    snow();
}
