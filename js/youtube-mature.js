jQuery(document).ready(function($) {
	var videoID = null;

	var embedVideo = function(videoID){
		// Clear any previous video
		$('.videocontainer').empty();
		// Build the iframe
		var iframe = $('<iframe/>').attr({
			'src': 'https://www.youtube.com/embed/'+videoID,
			'width': '853',
			'height': '480'
		});
		// Add a link back to YouTube
		var watchURL = 'https://www.youtube.com/watch?v='+videoID;
		var ytlink = $('<a href="'+watchURL+'">').html(watchURL);
		console.log(ytlink);
		// Stuff content to the page
		$('.videocontainer').append(iframe).css('display', 'block');
		$('.stdlink').append(ytlink).css('display', 'block');
	};

	// Handle input
	$('#videoForm').on('submit', function(event) {
		event.preventDefault();
		// Grab the video ID
		videoID = $('#videoIDinput').val();
		// Display
		embedVideo(videoID);
	});
});
