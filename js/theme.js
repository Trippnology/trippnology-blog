/**
 * Trippnology
 * http://trippnology.com
 *
 * Theme javascript
 */

(function ($) {
  'use strict';

  // Scroll to top button
  $().UItoTop({ easingType: 'easeOutQuart' });

  // Create clickable links from spam-proof email addresses
  $('body').antiSpam('antispam');

  // Smooth scroll to anchors on the page
  var scroller = function(id){
  	$('html, body').animate({
  		scrollTop: $(id).offset().top
  	}, 1000);
  };
  // Turn anchor links into smooth scrollers
  $('a.scrollme').click(function(){
  	var anchor = $(this).attr('href');
  	scroller(anchor);
  	return false;
  });

})(jQuery);
