<?php
/**
 * The template for displaying the prices page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div id="primary" class="col-sm-12 col-md-10 content-area">
			<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); // Just the title ?>

				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a class="scrollme" data-toggle="tab" href="#web-design">Web design and development</a></li>
					<li><a class="scrollme" data-toggle="tab" href="#seo">SEO</a></li>
					<li><a class="scrollme" data-toggle="tab" href="#web-hosting">Web hosting</a></li>
					<li><a class="scrollme" data-toggle="tab" href="#domain-names">Domain names</a></li>
					<li><a class="scrollme" data-toggle="tab" href="#it-support">IT Support</a></li>
				</ul>

				<section id="web-design">
					<h1>Web design and development</h1>
					<p><a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>">Web design</a>, updates, ammendments, technical changes etc. - <strong>&pound;60</strong> per hour.</p>
					<p>Due to it's bespoke nature, <a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>">website design</a> quotes are based on your individual requirements. We always start with a <a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>#project-assessment">project assessment</a> which includes an hour meeting with you and then a full breakdown of the work to be carried out. This service is initially billed at <strong>&pound;100</strong> but is included free of charge if you go ahead with the project.</p>
					<p>Please <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a> for a chat, we would love to hear your plans.</p>
					<?php //get_template_part( 'template-parts/cost-table' ); ?>
					<h2>Other web services</h2>
					<p>Performance review of your existing website - <strong>&pound;100</strong></p>
				</section>

				<section id="seo">
					<h1>SEO</h1>
					<p>SEO review of your existing website - <strong>&pound;100</strong></p>
					<p>All <a href="<?php echo esc_url( home_url( '/seo/' ) ); ?>">search engine optimisation</a> work is charged at our standard web service rate - <strong>&pound;60</strong> per hour.</p>
				</section>

				<hr class="divider large roundsm">

				<section id="web-hosting">
					<h1>Web Hosting</h1>
					<p>Confused? Read our blog post: <a href="<?php echo esc_url( home_url( '/what-is-web-hosting-and-why-do-i-need-it/' ) ); ?>">What is web hosting and why do I need it?</a></p>
					<p>We offer 12 months FREE hosting when we <a href="<?php echo esc_url( home_url( '/web-design/' ) ); ?>">build your website</a>.</p>
					<p><span class="lead">Simple Hosting</span><br>Our all-in-one web hosting solution suitable for most small to medium sized businesses.</p>
					<div class="row">
						<div class="col-lg-8">
							<table class="table table-condensed">
								<tr>
									<th>Webspace</th>
									<td>Unlimited</td>
								</tr>
								<tr>
									<th>Monthly traffic</th>
									<td>Unlimited</td>
								</tr>
								<tr>
									<th>Programming</th>
									<td>PHP 5 or 7, PHPDev, NodeJS, Perl, Python, Ruby, SSI</td>
								</tr>
								<tr>
									<th>Geo-redundancy</th>
									<td>Your data is stored simultaneously in two high-tech data centres in two separate locations. Double security through geo-redundancy!</td>
								</tr>
								<tr>
									<th>Database</th>
									<td>MySQL / MongoDB</td>
								</tr>
								<tr>
									<th>Secure Shell Access (SSH)</th>
									<td>
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										<span class="sr-only">Yes</span>
									</td>
								</tr>
								<tr>
									<th>Version control</th>
									<td>
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										<span class="sr-only">Yes</span>
									</td>
								</tr>
								<tr>
									<th>Subdomains</th>
									<td>Optional</td>
								</tr>
								<tr>
									<th>Dedicated SSL certificate</th>
									<td>Optional</td>
								</tr>
								<tr>
									<th>IMAP/POP3 Email accounts</th>
									<td>Unlimited 2GB accounts</td>
								</tr>
							</table>
						</div>
					</div>
					<p class="lead">All this for just <strong>&pound;6.99 per month</strong> with <strong>FREE setup</strong>.</p>
					<p>Other options (PAAS, VPS, dedicated server) available <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">on request</a>.</p>
				</section>

				<hr class="divider large roundsm">

				<section id="domain-names">
					<h1>Domain names</h1>
					<ul class="list-unstyled">
						<li>.co.uk - <strong>&pound;20 per 2 years</strong></li>
						<li>.com / .net / .org - <strong>&pound;20 per 1 year</strong></li>
						<li>Others - <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">price on application</a></li>
					</ul>
				</section>

				<hr class="divider large roundsm">

				<div class="row">
					<div class="col-sm-12">
						<p class="lead text-center" style="margin:0;">While our pricing is good, our customer service is outstanding. <br class="hidden-md hidden-lg"> <a href="<?php echo esc_url( home_url( '/testimonials/' ) ); ?>" class="btn btn-default btn-sm">Our customers agree!</a></p>
					</div>
				</div>

				<hr class="divider large roundsm">

				<section id="it-support">
					<h1>Ad-hoc IT support and computer repair</h1>
					<p><strong>Business Hours</strong> (09:00-18:00 Monday-Friday)<br>
					Domestic customers: <strong>&pound;40 Callout</strong><a href="#callout" class="tooltip-top" title="Includes first hour. Callout charges may be higher than stated based on your location - please double check with us first.">*</a> thereafter <strong>&pound;30 per hour</strong><a href="#hourly" class="tooltip-top" title="Billed in 15 minute intervals.">**</a>.<br>
					Commercial customers: <strong>&pound;60 Callout</strong><a href="#callout" class="tooltip-top" title="Includes first hour. Callout charges may be higher than stated based on your location - please double check with us first.">*</a> thereafter <strong>&pound;50 per hour</strong><a href="#hourly" class="tooltip-top" title="Billed in 15 minute intervals.">**</a>.</p>
					<p><strong>Outside Business Hours</strong> (18:00-09:00 or weekends)<br>
					Domestic customers: <strong>&pound;55 Callout</strong><a href="#callout" class="tooltip-top" title="Includes first hour. Callout charges may be higher than stated based on your location - please double check with us first.">*</a> thereafter <strong>&pound;50 per hour</strong><a href="#hourly" class="tooltip-top" title="Billed in 15 minute intervals.">**</a>.<br>
					Commercial customers: <strong>&pound;85 Callout</strong><a href="#callout" class="tooltip-top" title="Includes first hour. Callout charges may be higher than stated based on your location - please double check with us first.">*</a> thereafter <strong>&pound;70 per hour</strong><a href="#hourly" class="tooltip-top" title="Billed in 15 minute intervals.">**</a>.</p>

					<h2>Support contracts</h2>
					<p>Packages as detailed on the <a href="<?php echo esc_url( home_url( '/it-support/' ) ); ?>">IT Support</a> page. Prices are based on the total number of devices<a href="#devices" class="tooltip-top" title="The following items count as a Device: Desktop PC (including monitor), Laptop, Server, Printer, Scanner, Router, Hub, Wireless Access Point and other similar items.">**</a> to be covered. </p>
					<p>Remote Only Support<br>
					Up to 10 - <strong>&pound;80 per month </strong><br>
					Up to 20 - <strong>&pound;150 per month </strong><br>
					More than 20 - Please <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a>.</p>
					<p>Full On Site Support <br>
					Up to 10 - <strong>&pound;150 per month </strong><br>
					Up to 20 - <strong>&pound;280 per month </strong><br>
					More than 20 - Please <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a>.</p>
					<p>24/7 On Site Support<br>
					Up to 10 - <strong>&pound;250 per month </strong><br>
					Up to 20 - <strong>&pound;475 per month </strong><br>
					More than 20 - Please <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a>.</p>

					<h3>Notes</h3>
					<p id="callout">* Includes first hour. Callout charges may be higher than stated based on your location - please double check with us first.</p>
					<p id="hourly">** Billed in 15 minute intervals</p>
					<p id="devices">*** The following items count as a Device: Desktop PC (<strong>including</strong> monitor), Laptop, Server, Printer, Scanner, Router, Hub, Wireless Access Point and other similar items. </p>
				</section>

			<?php endwhile; // End of the loop. ?>

			</main>
		</div><!-- #primary -->
	</div>
</div>
<?php get_footer(); ?>
