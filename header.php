<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Trippnology
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php get_template_part('template-parts/preconnect'); ?>
<?php get_template_part('template-parts/fonts'); ?>
<?php wp_head(); ?>
<?php get_template_part('template-parts/favicons'); ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'trippnology' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-sm-push-9 col-lg-2 col-lg-push-10 logo-column">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemscope="" itemtype="http://schema.org/Brand">
						<img
							srcset="https://cdn.trippnology.com/img/trippnology-logo-square-180.png 180w,
									https://cdn.trippnology.com/img/trippnology-logo-square-360.png 360w"
							sizes="165px"
							src="https://cdn.trippnology.com/img/trippnology-logo-square-180.png"
							alt="Trippnology logo" class="logo" itemprop="logo">
					</a>
				</div>
				<div class="col-sm-9 col-sm-pull-3 col-lg-10 col-lg-pull-2 site-branding">
					<!-- <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> -->
					<?php
					if ( is_front_page() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
					endif;
					?>
					<p class="site-description">
						You love your business, we love the web. <br class="hidden-md hidden-lg">Let's get together and make something great!
						<?php //bloginfo( 'description' ); ?>
					</p>
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<button class="btn btn-default menu-toggle" aria-controls="" aria-expanded="false"><?php esc_html_e( 'Menu', 'trippnology' ); ?></button>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_id' => 'primary-menu',
							'menu_class' => 'nav nav-pills nav-justified'
						) );
						if (!is_page()) {
							wp_nav_menu( array(
								'theme_location' => 'blog',
								'menu_id' => 'blog-menu',
								'menu_class' => 'nav nav-pills nav-justified'
							) );
						} ?>
					</nav><!-- #site-navigation -->
				</div><!-- .site-branding -->
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
