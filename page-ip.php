<?php
/**
 * Provides information about the user's IP address
 *
 * @package Trippnology
 */

$host = $_SERVER['REMOTE_HOST'];
$format = $_GET['format'];
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
$userIP = $_SERVER['REMOTE_ADDR'];

/* If the "format" query parameter is used, simply return the IP
   and forget about rendering the rest of the page */
if ($format == "text") {
	header('Content-Type: text/plain; charset=utf8');
	echo $userIP;
	die;
}
if ($format == "json") {
	header('Content-Type: application/json; charset=utf8');
	echo json_encode(array('ip' => $userIP));
	die;
}
if ($format == "jsonp") {
	header('Content-Type: text/javascript; charset=utf8');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Max-Age: 3628800');
	header('Access-Control-Allow-Methods: GET');
	$callback = $_GET['callback'];
	echo $callback . '(' . json_encode(array('ip' => $userIP)) . ');';
	die;
}

get_header(); ?>
<div class="container">
	<div class="row">
		<div id="primary" class="col-sm-12 content-area">
			<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'ip' ); // Just the title ?>

				<hr class="divider large roundsm">

			<?php endwhile; // End of the loop. ?>

			</main>
		</div><!-- #primary -->
	</div>
</div>
<?php get_footer(); ?>
<script>
	$(function() {
		// Check for IPv6 connectivity by trying to load an image
		// from a domain that is IPv6 only
		var img = new Image();

		$(img).load(function() {
			var link = $('<a href="https://ipv6.trippnology.com">');
			$(link).append(this);
			$('#ipv6-status').html(link);
		})
		.attr({
			alt: 'IPv6 ready',
			src: 'https://ipv6.trippnology.com/img/ipv6-80x15.png'
		})
		.error(function() {
			$('#ipv6-status').text('It looks like you are unable to access the IPv6 internet.');
		});
	});
</script>
