<?php
/**
 * A blank page template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Trippnology
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div id="primary" class="col-sm-12 content-area">
			<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); // Just the title ?>

				<div class="row">
					<div class="col-sm-12">

					</div>
				</div>

				<hr class="divider large roundsm">

			<?php endwhile; // End of the loop. ?>

			</main>
		</div><!-- #primary -->
	</div>
</div>
<?php get_footer(); ?>
